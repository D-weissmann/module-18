#Module 18

# Overview
This is analysis to attempt to predict which loans will be risky and which will not. We will be using several other factors. 

# Analysis
We used the following 6 different models and they had the following scores:
1. The Random Oversampler:
    - Balanced Accuracy Score: 65.9%
    - Precision Score:99%
    - Recal Score:62%

2. SMOTE
    - Balanced Accuracy Score: 67.1%
    - Precision Score:99%
    - Recal Score:65%

3. Cluster Centroids
    - Balanced Accuracy Score:53.1% 
    - Precision Score:99%
    - Recal Score:47%
    
4. SMOTEEN
    - Balanced Accuracy Score: 65.9%
    - Precision Score:99%
    - Recal Score:60%
    
5. Balanced Random Forest Classifier
    - Balanced Accuracy Score: 81.6%
    - Precision Score:99%
    - Recal Score:91%
    
6. Easy Ensemble Classifier
    - Balanced Accuracy Score: 93.2%
    - Precision Score:99%
    - Recal Score:95%


# Summary
It would appear as if the Easy Ensemble Classifier is the best model to use for this dataset, with all of the scores being far above the other options. The Cluster Centroids model is the worst with a lower Accuracy and Recal score than any of the others. 
